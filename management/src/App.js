import React, { Component } from 'react';
import Base from './components/Base';
import Bar from './components/Bar';
import List from './components/List';
import Paint from './components/Paint';


const colors = ['#A859FF', '#7051E8', '#666FFF', '#5183E8'];

class App extends Component {

  id = 3 

  state = {
    input: '',
    todos: [
      { id: 0, text: 'Homework', checked: false },
      { id: 1, text: 'Final Project', checked: true },
      { id: 2, text: 'Work Out', checked: false },
    ],
    color: '#A859FF'
  }

  handleChange = (e) => {
    this.setState({
      input: e.target.value 
    });
  }

  handleCreate = () => {
    const { input, todos, color } = this.state;
    this.setState({
      input: '', 
    
      todos: todos.concat({
        id: this.id++,
        text: input,
        checked: false,
        color
      })
    });
  }

  handleKeyPress = (e) => {

    if(e.key === 'Enter') {
      this.handleCreate();
    }
  }

  handleToggle = (id) => {
    const { todos } = this.state;
    
    const index = todos.findIndex(todo => todo.id === id);
    const selected = todos[index]; 

    const nextTodos = [...todos]; 
  
    nextTodos[index] = { 
      ...selected, 
      checked: !selected.checked
    };

    this.setState({
      todos: nextTodos
    });
  }

  handleRemove = (id) => {
    const { todos } = this.state;
    this.setState({
      todos: todos.filter(todo => todo.id !== id)
    });
  }

  handleSelectColor = (color) => {
    this.setState({
      color
    })
  }

  render() {
    const { input, todos, color } = this.state;
    const {
      handleChange,
      handleCreate,
      handleKeyPress,
      handleToggle,
      handleRemove,
      handleSelectColor
    } = this;

    return (
      <Base Bar={(
        <Bar 
          value={input}
          onKeyPress={handleKeyPress}
          onChange={handleChange}
          onCreate={handleCreate}
          color={color}
        />
      )}
        Paint={(
          <Paint colors={colors} selected={color} onSelect={handleSelectColor}/>
        )}>
        <List todos={todos} onToggle={handleToggle} onRemove={handleRemove}/>
      </Base>
    );
  }
}

export default App;
