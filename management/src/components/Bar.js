import React from 'react';
import './Bar.css';

const Bar = ({value, onChange, onCreate, onKeyPress, color}) => {
  return (
    <div className="key">
      <input value={value} onChange={onChange} onKeyPress={onKeyPress} style={{color}}/>
      <div className="button" onClick={onCreate}>
        add
      </div>
    </div>
  );
};

export default Bar;