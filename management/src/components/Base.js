import React from 'react';
import './Base.css';

const Base = ({Bar, Paint, children}) => {
  return (
    <main className="heading">
      <div className="title">
        To Do List
      </div>
      <section className="Paint-wrapper">
        {Paint}
      </section>
      <section className="Bar-wrapper">
        {Bar}
      </section>
      <section className="todos-wrapper">
        {children}
      </section>
    </main>
  );
};

export default Base;