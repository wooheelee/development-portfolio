import React, { Component } from 'react';
import Item from './Item';

class List extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.todos !== nextProps.todos;
  }
  
  render() {
    const { todos, onToggle, onRemove } = this.props;
    
    const List = todos.map(
      ({id, text, checked, color}) => (
        <Item
          id={id}
          text={text}
          checked={checked}
          color={color}
          onToggle={onToggle}
          onRemove={onRemove}
          key={id}
        />
      )
    );

    return (
      <div>
        {List}    
      </div>
    );
  }
}

export default List;